import "./Chart.css";
import ChartHistory from "./ChartHistory";

export const Chart = ({
  encodedUrl = "Syp9J4vLqBLJSCfFib9mB2t9ICqhoKnEBCdCprC8IYqiJIqkuGBAAUW2rO0LOr5LN92VLvpA1G00",
  chartHistory,
  plantuml,
  setPlantuml,
}) => {
  return (
    <div className="section-chart">
      <div className="chart">
        <img
          src={`${process.env.REACT_APP_NOT_PLANTUML_SERVER_URL}/svg/${encodedUrl}`}
          alt="src"
        />
      </div>
      <ChartHistory
        chartHistory={chartHistory}
        plantuml={plantuml}
        setPlantuml={setPlantuml}
      />
    </div>
  );
};
