import { useState } from "react";
import "./Writer.css";
import Editor from "react-simple-code-editor";
import { highlight, languages } from "prismjs/components/prism-core";

import ClipLoader from "react-spinners/ClipLoader";
import { FaUndo } from "react-icons/fa";
import { RiDeleteBin5Fill } from "react-icons/ri";
import "prismjs/components/prism-clike";
import "prismjs/components/prism-javascript";
import "prismjs/themes/prism.css"; //Example style, you can use another

const override = {
  display: "block",
  margin: "0 auto",
  borderColor: "darkorange",
};

export const Writer = ({
  plantuml,
  setPlantuml,
  chartHistory,
  setChartHistory,
  setIsWritingFocused,
  isLoading,
}) => {
  const handleClear = () => {
    setPlantuml("");
  };

  const handleUndo = () => {
    if (chartHistory.length > 0) {
      const chartHistoryLastElem = chartHistory.slice(-1);
      setPlantuml(chartHistoryLastElem[0].plantuml);
      setChartHistory(chartHistory.slice(0, -1));
    }
  };

  return (
    <>
      {isLoading ? (
        <div className="spinner">
          <ClipLoader
            color={"grey"}
            loading={isLoading}
            cssOverride={override}
            size={80}
            aria-label="Loading Spinner"
            data-testid="loader"
          />
        </div>
      ) : (
        <div className="writer-div">
          <div className="writer-btn-div">
            <button className="writer-btn undo" onClick={() => handleUndo()}>
              <FaUndo className="undo-icon" />
              Undo
            </button>
            <button className="writer-btn clear" onClick={() => handleClear()}>
              <RiDeleteBin5Fill />
              Clear
            </button>
          </div>
          <Editor
            value={plantuml}
            onFocus={() => setIsWritingFocused(true)}
            onBlur={() => setIsWritingFocused(false)}
            onValueChange={(plantuml) => setPlantuml(plantuml)}
            className="writer"
            highlight={(plantuml) => highlight(plantuml, languages.js)}
            padding={20}
            style={{
              fontFamily: '"Fira code", "Fira Mono", monospace',
              fontSize: 15,
            }}
          />
        </div>
      )}
    </>
  );
};
