import React from "react";
import "./ChartHistory.css";

const ChartHistory = ({ chartHistory, plantuml, setPlantuml }) => {
  const chartHistoryLast4 = chartHistory.slice(-4);

  const handleHistory = (plantuml) => setPlantuml(plantuml);

  return (
    <div className={`chartHistory ${chartHistory.length == 1 && "hide"}`}>
      <p className="chartHistory-title">Previous Results</p>
      <div className="chartHistory-grid">
        {chartHistoryLast4.map((item) => {
          if (plantuml != item.plantuml) {
            return (
              <button
                className="btn-history"
                onClick={() => handleHistory(item.plantuml)}
              >
                <img
                  src={`${process.env.REACT_APP_NOT_PLANTUML_SERVER_URL}/png/${item.img}`}
                  alt="src"
                />
              </button>
            );
          }
        })}
      </div>
    </div>
  );
};

export default ChartHistory;
