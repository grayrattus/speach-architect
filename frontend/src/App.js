import "./App.css";
import { Writer } from "./components/Writer";
import { Chart } from "./components/Chart";
import { useEffect, useRef, useState } from "react";
import PlantUmlEncoder from "plantuml-encoder";
import { useReactMediaRecorder } from "react-media-recorder";
import { cleanup } from "@testing-library/react";
import Editor from "react-simple-code-editor";

const defaultPlantUml = `Person(person, "Label", "Optional Description")
Container(product, "Label", "Technology", "Optional Description")
System(database, "Label", "Optional Description")

Rel(product, database, "Label", "Optional Technology")
`;

/* 

@enduml
*/

const START_UML = `@startuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

`;

const END_UML = `
@enduml`;

const handlePlantUml = (plantuml) => {
  return START_UML + plantuml + END_UML;
};

function App() {
  const [plantuml, setPlantuml] = useState(defaultPlantUml);
  const [backendResponse, setBackendResponse] = useState(null);
  const [isRecording, setIsRecording] = useState(false);
  const [isWritingFocused, setIsWritingFocused] = useState(false);
  const [chartHistory, setChartHistory] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const backendText = useRef(null);

  const encodedUrl = PlantUmlEncoder.encode(handlePlantUml(plantuml));

  const { status, startRecording, stopRecording, mediaBlobUrl } =
    useReactMediaRecorder({
      audio: true,
      mediaRecorderOptions: { mimeType: "audio/wav" },
    });

  useEffect(() => {
    setIsRecording(status === "recording" ? true : false);
  }, [status]);

  useEffect(() => {
    download();
  }, [mediaBlobUrl]);

  useEffect(() => {
    const timer = setTimeout(() => {
      const historyItem = {
        plantuml: plantuml,
        img: encodedUrl,
      };
      setChartHistory([...chartHistory, historyItem]);
    }, 1000);
    return () => clearTimeout(timer);
  }, [encodedUrl]);

  useEffect(() => {
    document.addEventListener("keydown", detectKeyDown);

    return () => {
      document.removeEventListener("keydown", detectKeyDown);
    };
  }, [isRecording, isWritingFocused]);

  const detectKeyDown = (e) => {
    console.log("Clicked key:", e.key);

    if (e.code === "Space" && !isWritingFocused) {
      console.log("status", status);
      isRecording ? stopRecording() : startRecording();
    }
  };

  const download = async (b) => {
    const formData = new FormData();
    const res = await fetch(mediaBlobUrl);
    const blob = await res.blob();

    const audiofile = new File([blob], "audiofile.wav");
    formData.append("audioFile", audiofile);
    formData.append("plantUmlString", plantuml);

    try {
      setIsLoading(true);
      const response = await fetch("http://localhost:8081/upload", {
        method: "POST",
        body: formData,
      });

      if (response.status === 200) {
        const puml = await response.text();
        setPlantuml(puml);
        setBackendResponse(null);
      } else if (response.status === 303) {
        const puml = await response.text();
        setBackendResponse(puml);
      }
    } catch (error) {
      console.log(error);
    } finally {
      setIsLoading(false);
    }
  };

  const updatePlantUmlFromUserChange = async () => {
    const formData = new FormData();
    formData.append("userString", backendResponse);
    formData.append("plantUmlString", plantuml);
    setIsLoading(true);
    const response = await fetch("http://localhost:8081/upload-text", {
      method: "POST",
      body: formData,
    });

    if (response.status === 200) {
      const puml = await response.text();
      setPlantuml(puml);
      setBackendResponse(null);
    } else if (response.status === 303) {
      const puml = await response.text();
      setBackendResponse(puml);
    } 
    setIsLoading(false);
  }

  return (
    <div className="App">
      <div className="screen">
        <div className="half-plantuml">
          <Writer
            setIsWritingFocused={setIsWritingFocused}
            plantuml={plantuml}
            setPlantuml={setPlantuml}
            chartHistory={chartHistory}
            setChartHistory={setChartHistory}
            isLoading={isLoading}
          />
        </div>
        <div className="half-encodeurl">
          <Chart
            encodedUrl={encodedUrl}
            chartHistory={chartHistory}
            plantuml={plantuml}
            setPlantuml={setPlantuml}
          />
        </div>
      </div>
      {backendResponse ?
        <div className="backend-response">
          <><input
            className="backend-response__input"
            value={backendResponse}
            onFocus={() => setIsWritingFocused(true)}
            onBlur={() => setIsWritingFocused(false)}
            onChange={(p) => setBackendResponse(p.target.value)}
          />
            <button onClick={updatePlantUmlFromUserChange}>Accept change</button>
          </>
        </div> : null}
      <div className="section-record">
        <div className="rec">
          <span className={`on-record ${isRecording && "recording"}`}></span>
          <p className="label-record">REC</p>
        </div>
        <button
          className={`btn-record ${isRecording && "recording"}`}
          onClick={
            status === "recording"
              ? () => stopRecording()
              : () => startRecording()
          }
        ></button>
      </div>
    </div>
  );
}

export default App;
