<h1 style="color:darkorange;"> Speech-to-UML </h1>

## Create Diagrams using Human Language

---

<h1 style="color:darkorange;"> Story behind it...</h1> 
Note: 
Our project deals with multiple stakeholders and we need to build a datamodel for each one of them
Architecture department

---

<!-- .slide: data-background="https://www.orencloud.com/wp-content/uploads/2023/02/ChatGPT-Feature-1024x576.webp" -->

---

<!-- .slide: data-background="https://i.imgur.com/zMGkiaw.png" -->

<h1 style="color:black;"> We don't use it!</h1>

---

<h2 style="color:orange;text-transform:inherit;">Our Architecture</h2>

![alt text](http://www.plantuml.com/plantuml/svg/RPB1RXiX48RlFCNKEVIqJff33pvKWNNLIorPSjCxXSakCcQs0sixVVg2DycIo9QbbVx_O_X6uY4cpMayUF7vaml6ZnPXy3gKgD6uTAKISU_2e5aV8EBV4OD1oKodNxqp-u0nmds5C8t16kA03FTtLN7GvqE6TOVH_KFuUYVgoTliuOmRyjrRRQqBtVlJYiwl-PSwtxqOvLvfi-ykrxVaYK5wbtC_OeLBauWLiKMUeFpRvRaULTueP2LT27YCo73phlb3PrXiTESCz7eOC4JeaN4XXbaT5hk1iwCYIbcDZyWBq14at9nwb_0ZYsZw9b6Jy9nWGnRz1LTNyQtc-3AlVGKmMCXYuWFfy-TnyxDMM0jnMQhv1d859ts49-UnhEkEEY588MGfrRLI2LMwTWMJCr20uYmipioCjo1QQAQRYeOXYq9Uak9GXTLvEGkkL_0zbDxEv1kt5kjgqkxtZydLOaLKptcvlCt_jqSTqxF7UAknCJGyi-lwr3mH9UHboliyOB0P-my0 "Title")

---

<h2 style="color:orange;text-transform:inherit;">Investiment for the future...</h2>

Our Formula: Fully open-source + Community models + Self hosted

<p class="fragment fade-up"><span style="color:red;font-weight:bold;">No risk</span> for ING</p>
<p class="fragment fade-up"><span style="color:red;font-weight:bold;">Free</span> of cost with vendors</p>
<p class="fragment fade-up">Code is provided and <span style="color:red;font-weight:bold;">easy to maintain</span></p>
<p class="fragment fade-up"><span style="color:red;font-weight:bold;">1 month</span> to reach production</p>

---

<h1 style="color:darkorange;">Demo time!</h1>