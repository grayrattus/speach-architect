/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.app.SpeachBackend;

import ca.uqac.lif.bullwinkle.BnfParser;
import ca.uqac.lif.bullwinkle.ParseTreeObjectBuilder;
import java.io.FileNotFoundException;

/**
 *
 * @author grayrattus
 */
public interface TextToPlantUml {

    public PlantUmlParsed parseTextToPlantUmlText(String text) throws FileNotFoundException, BnfParser.InvalidGrammarException, BnfParser.ParseException, ParseTreeObjectBuilder.BuildException;

}
