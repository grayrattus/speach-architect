/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.app.SpeachBackend;

import ca.uqac.lif.bullwinkle.BnfParser;
import ca.uqac.lif.bullwinkle.Builds;
import ca.uqac.lif.bullwinkle.ParseNode;
import ca.uqac.lif.bullwinkle.ParseTreeObjectBuilder;
import static com.app.SpeachBackend.SpeachActionOfPlantUml.CREATE;
import static com.app.SpeachBackend.SpeachActionOfPlantUml.NOT_SET;
import static com.app.SpeachBackend.SpeachActionOfPlantUml.RELATION;
import com.tagtraum.ffsampledsp.FFAudioFileReader;

import java.io.*;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import main.java.com.goxr3plus.jsfggrammarparser.parser.JSGFGrammarParser;
import main.java.com.goxr3plus.jsfggrammarparser.test.Tester;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.vosk.LibVosk;
import org.vosk.LogLevel;
import org.vosk.Model;
import org.vosk.Recognizer;
import org.json.*;

import io.micrometer.common.util.StringUtils;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class HelloController {

    Model model;
    List<TextToPlantUml> textToPlantUmlServices;

    public HelloController() throws IOException, BnfParser.InvalidGrammarException, BnfParser.ParseException, ParseTreeObjectBuilder.BuildException {
        // this.model = new Model("./assets/vosk-model-small-en-us-0.15");
        // this.model = new Model("./assets/vosk-model-en-us-0.42-gigaspeech");
        // this.model = new Model("./assets/vosk-model-en-us-0.22-lgraph");
        this.model = new Model("./assets/vosk-model-en-us-0.22");
        // this.model = new Model("./assets/vosk-model-en-us-daanzu-20200905");
        this.textToPlantUmlServices = new LinkedList<>();
        this.textToPlantUmlServices.add(new TextToPlantUmlCreate());
        this.textToPlantUmlServices.add(new TextToPlantUmlRelation());
        this.textToPlantUmlServices.add(new TextToPlantUmlRelationSelf());
    }

    @PostMapping("/upload")
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<String> uploadFile(@RequestParam("audioFile") MultipartFile file, @RequestParam("plantUmlString") String plantUmlString) throws UnsupportedAudioFileException, FileNotFoundException, BnfParser.InvalidGrammarException, BnfParser.ParseException, ParseTreeObjectBuilder.BuildException {
        try {
            // Do something with the uploaded file
            byte[] fileBytes = file.getBytes();

            try (InputStream ais = AudioSystem.getAudioInputStream(new BufferedInputStream(file.getInputStream())); Recognizer recognizer = new Recognizer(this.model, 48000 * 2)) {
                int nbytes;
                recognizer.setWords(true);
                byte[] b = new byte[4096];
                while ((nbytes = ais.read(b)) >= 0) {

                    if (recognizer.acceptWaveForm(b, nbytes)) {
                        // System.out.println(recognizer.getResult());
                    } else {
                        // System.out.println(recognizer.getPartialResult());
                    }
                }
                var finalResult = new JSONObject(recognizer.getFinalResult());
                System.out.println("User spoke: " + finalResult.get("text"));
                PlantUmlParsed result = new PlantUmlParsed(SpeachActionOfPlantUml.NOT_SET, "");
                for (int i = 0; i < this.textToPlantUmlServices.size(); i++) {
                    try {
                        result = this.textToPlantUmlServices.get(i).parseTextToPlantUmlText(finalResult.get("text").toString());
                    } catch (ParseTreeObjectBuilder.BuildException | FileNotFoundException | BnfParser.InvalidGrammarException | JSONException | BnfParser.ParseException be) {
                        System.out.println("Parser filed, another one is picked");
                    }
                }

                String returnBody = "";

                System.out.println("Result: " + result.string);
                switch (result.action) {
                    case CREATE:
                        returnBody += result.string + "\n";
                        returnBody += plantUmlString;
                        break;
                    case RELATION:
                        returnBody += plantUmlString + "\n" + result.string;
                        break;
                    case NOT_SET:
                        returnBody += plantUmlString;
                        break;
                    default:
                        returnBody += plantUmlString;
                        break;
                }

                if (result.action == NOT_SET) {
                    return ResponseEntity.status(HttpStatus.SEE_OTHER).body(finalResult.get("text").toString());
                }
                return ResponseEntity.status(HttpStatus.OK).body(returnBody);

            }
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to upload file.");
        }
    }
    @PostMapping("/upload-text")
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<String> uploadText(@RequestParam("userString") String userString, @RequestParam("plantUmlString") String plantUmlString) throws UnsupportedAudioFileException, FileNotFoundException, BnfParser.InvalidGrammarException, BnfParser.ParseException, ParseTreeObjectBuilder.BuildException {
        // Do something with the uploaded file
        System.out.println("User wrote: " + userString);
        PlantUmlParsed result = new PlantUmlParsed(SpeachActionOfPlantUml.NOT_SET, "");
        for (int i = 0; i < this.textToPlantUmlServices.size(); i++) {
            try {
                result = this.textToPlantUmlServices.get(i).parseTextToPlantUmlText(userString);
            } catch (ParseTreeObjectBuilder.BuildException | FileNotFoundException | BnfParser.InvalidGrammarException | JSONException | BnfParser.ParseException be) {
                System.out.println("Parser filed, another one is picked");
            }
        }

        String returnBody = "";

        System.out.println("Result: " + result.string);
        switch (result.action) {
            case CREATE:
                returnBody += result.string + "\n";
                returnBody += plantUmlString;
                break;
            case RELATION:
                returnBody += plantUmlString + "\n" + result.string;
                break;
            case NOT_SET:
                returnBody += plantUmlString;
                break;
            default:
                returnBody += plantUmlString;
                break;
        }
        return ResponseEntity.status(HttpStatus.OK).body(returnBody);

    }
}
