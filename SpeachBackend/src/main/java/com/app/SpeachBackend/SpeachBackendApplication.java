package com.app.SpeachBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpeachBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpeachBackendApplication.class, args);
	}

}
