/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.app.SpeachBackend;

import ca.uqac.lif.bullwinkle.BnfParser;
import ca.uqac.lif.bullwinkle.Builds;
import ca.uqac.lif.bullwinkle.ParseTreeObjectBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author grayrattus
 */
public class TextToPlantUmlAdvancedSentence implements TextToPlantUml {

    @Override
    public PlantUmlParsed parseTextToPlantUmlText(String text) throws FileNotFoundException, BnfParser.InvalidGrammarException, BnfParser.ParseException, ParseTreeObjectBuilder.BuildException {
        BnfParser parser = new BnfParser(new FileInputStream(new File("./assets/advanced-sentence.bnf")));
        parser.setDebugMode(true);
        var tree = parser.parse(text);
        var builder = new CreateBuilder();
        var exp = builder.build(tree);
        return new PlantUmlParsed(builder.action, builder.plantUmlString);
    }

    public static class CreateBuilder extends ParseTreeObjectBuilder<String> {

        public String plantUmlString = "";
        private String entity = "";
        public SpeachActionOfPlantUml action = SpeachActionOfPlantUml.NOT_SET;

        @Builds(rule = "<actor>")
        public void buildActor(Deque<Object> stack) {
            for (int i = 0; i <= stack.size(); i++) {
                stack.pop();
            }
            this.plantUmlString += "Person(";
        }

        @Builds(rule = "<verb>")
        public void buildCreate(Deque<Object> stack) {
            this.action = SpeachActionOfPlantUml.CREATE;
        }

        @Builds(rule = "<container>")
        public void buildContainer(Deque<Object> stack) {
            for (int i = 0; i <= stack.size(); i++) {
                stack.pop();
            }
            this.plantUmlString += "Container(";
        }

        @Builds(rule = "<argument>")
        public void buildArgument(Deque<Object> stack) {
            var value = stack.pop();

            this.plantUmlString += ",\"Label\",\"" + value + "\")";
        }

        @Builds(rule = "<system>")
        public void buildSystem(Deque<Object> stack) {
            for (int i = 0; i <= stack.size(); i++) {
                stack.pop();
            }
            this.plantUmlString += "System(";
        }

        @Builds(rule = "<entityName>")
        public void buildEntityName(Deque<Object> stack) {
            var entityName = stack.pop();

            for (int i = 0; i <= stack.size(); i++) {
                stack.pop();
            }
            this.entity = entityName.toString();
            this.plantUmlString += entityName.toString();
        }

        @Builds(rule = "<createActorWithoutDescription>")
        public void buildCreateActorWithoutDescription(Deque<Object> stack) {
            this.plantUmlString += ",\"" + this.entity + "\")";
        }

        @Builds(rule = "<createSystemWithoutDescription>")
        public void buildCreateSystemWithoutDescription(Deque<Object> stack) {
            this.plantUmlString += ",\"" + this.entity + "\")";
        }

        @Builds(rule = "<createContainerWithoutDescription>")
        public void buildCreateContainerWithoutDescription(Deque<Object> stack) {
            this.plantUmlString += ",\"" + this.entity + "\")";
        }

    }
}
