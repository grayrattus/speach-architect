/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.app.SpeachBackend;

public abstract class PlantUMLExpression {

    public static abstract class CreateActorPlantUMLExpression extends PlantUMLExpression {

        String command="";

        public CreateActorPlantUMLExpression(String command) {
            this.command = command;
        }

        @Override
        public String toString() {
            return this.command;
        }
    }

    public static class Type extends CreateActorPlantUMLExpression {

        public Type(String command) {
            super(command);
        }
    }

}
