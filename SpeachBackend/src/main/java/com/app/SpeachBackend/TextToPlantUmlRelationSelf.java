/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.app.SpeachBackend;

import ca.uqac.lif.bullwinkle.BnfParser;
import ca.uqac.lif.bullwinkle.Builds;
import ca.uqac.lif.bullwinkle.ParseTreeObjectBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

public class TextToPlantUmlRelationSelf implements TextToPlantUml {

    @Override
    public PlantUmlParsed parseTextToPlantUmlText(String text) throws FileNotFoundException, BnfParser.InvalidGrammarException, BnfParser.ParseException, ParseTreeObjectBuilder.BuildException {
        BnfParser parser = new BnfParser(new FileInputStream(new File("./assets/relation-self.bnf")));
        // parser.setDebugMode(true);
        var tree = parser.parse(text);
        var builder = new RelationBuilder();
        var exp = builder.build(tree);
        Map<SpeachActionOfPlantUml, String> returnMap = new HashMap<>();

        return new PlantUmlParsed(builder.action, builder.plantUmlString);
    }

    public static class RelationBuilder extends ParseTreeObjectBuilder<String> {

        public String plantUmlString = "";
        public SpeachActionOfPlantUml action = SpeachActionOfPlantUml.NOT_SET;
        public String fromRelation = "";
        public String toRelation = "";

        @Builds(rule = "<createRelation>")
        public void buildRelation(Deque<Object> stack) {
            this.action = SpeachActionOfPlantUml.RELATION;
            this.plantUmlString += "Rel(" + this.toRelation + "," + this.toRelation + ",\"Uses\")";
        }

        @Builds(rule = "<toRelation>")
        public void buildToRelation(Deque<Object> stack) {
            var value = stack.pop();
            this.toRelation = value.toString();
        }
    }

}
