/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.app.SpeachBackend;

/**
 *
 * @author grayrattus
 */
public class PlantUmlParsed {

    public SpeachActionOfPlantUml action;
    public String string;

    public PlantUmlParsed(SpeachActionOfPlantUml action, String string) {
        this.action = action;
        this.string = string;

    }
}
