/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.app.SpeachBackend;

import ca.uqac.lif.bullwinkle.BnfParser;
import ca.uqac.lif.bullwinkle.ParseTreeObjectBuilder;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author grayrattus
 */
public class TextToPlantUmlServiceRelationSelf {

    /**
     * Test of parseTextToPlantUmlText method, of class TextToPlantUmlService.
     */
    private static Map<String, String> inputOutput = new HashMap<>();

    @BeforeAll
    public static void init() {
        inputOutput.put("create self relation of server", "Rel(server,server,\"Uses\")");
        inputOutput.put("create self relation of product", "Rel(product,product,\"Uses\")");
        inputOutput.put("create self relation for product", "Rel(product,product,\"Uses\")");
        inputOutput.put("create self relationship for product", "Rel(product,product,\"Uses\")");
        inputOutput.put("create self relation for database", "Rel(database,database,\"Uses\")");
    }

    @Test
    public void testParseTextToPlantUmlTextt() {
        TextToPlantUmlRelationSelf instance = new TextToPlantUmlRelationSelf();

        inputOutput.forEach((input, output) -> {
            try {
                assertEquals(output, instance.parseTextToPlantUmlText(input).string);
            } catch (FileNotFoundException e) {
                //assertEquals(true, false);
                throw new RuntimeException(e);
            } catch (BnfParser.InvalidGrammarException e) {
                assertEquals(true, false);

                throw new RuntimeException(e);
            } catch (BnfParser.ParseException e) {
                assertEquals(true, false);

                throw new RuntimeException(e);
            } catch (ParseTreeObjectBuilder.BuildException e) {
                assertEquals(true, false);

                throw new RuntimeException(e);
            }
        });
    }

}
