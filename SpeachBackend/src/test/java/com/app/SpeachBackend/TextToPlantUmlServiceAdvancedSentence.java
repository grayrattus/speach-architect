/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.app.SpeachBackend;

import ca.uqac.lif.bullwinkle.BnfParser;
import ca.uqac.lif.bullwinkle.ParseTreeObjectBuilder;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author grayrattus
 */
public class TextToPlantUmlServiceAdvancedSentence {

    /**
     * Test of parseTextToPlantUmlText method, of class TextToPlantUmlService.
     */
    private static Map<String, String> inputOutput = new HashMap<>();

    @BeforeAll
    public static void init() {
        // inputOutput.put("create actor named hello that has description hello world", "Person(hello,\"Label\",\"hello world\")");
        // inputOutput.put("make me an system having name hello that has description hello world", "System(hello,\"Label\",\"hello world\")");
        // inputOutput.put("build me an actor named hello and assign it a description of hello world", "Person(hello,\"Label\",\"hello world\")");
        // inputOutput.put("generate me actor with name hello and a description of hello world", "Person(hello,\"Label\",\"hello world\")");
        // inputOutput.put("formulate an actor named hello and describe it with hello world", "Person(hello,\"Label\",\"hello world\")");
        // inputOutput.put("craft an actor with name hello and a description that specifies hello world", "Person(hello,\"Label\",\"hello world\")");
        // inputOutput.put("construct an actor named hello with a description of hello world", "Person(hello,\"Label\",\"hello world\")");
        // inputOutput.put("create actor having name hello", "Person(hello,\"hello\")");
        // inputOutput.put("create system having name hello", "System(hello,\"hello\")");
        // inputOutput.put("create container having name hello", "Container(hello,\"hello\")");
    }

    @Test
    public void testParseTextToPlantUmlText() {
        TextToPlantUmlAdvancedSentence instance = new TextToPlantUmlAdvancedSentence();

        inputOutput.forEach((input, output) -> {
            try {
                assertEquals(output, instance.parseTextToPlantUmlText(input).string);
            } catch (FileNotFoundException e) {
                //assertEquals(true, false);
                throw new RuntimeException(e);
            } catch (BnfParser.InvalidGrammarException e) {
                System.out.println("Invalid grammar" + e.toString());
                assertEquals(true, false);
                throw new RuntimeException(e);
            } catch (BnfParser.ParseException e) {
                assertEquals(true, false);

                throw new RuntimeException(e);
            } catch (ParseTreeObjectBuilder.BuildException e) {
                assertEquals(true, false);

                throw new RuntimeException(e);
            }
        });
    }

}
