# Introduction

Application is made to show PoC of speach to PlantUML (C4 model) abilities.

# To setup

1. Start docker of plantUML server
```
sh start_docker.sh
```

2. Run backend
```
# Download VOSK Api model

cd SpeachBackend/
curl https://alphacephei.com/vosk/models/vosk-model-small-en-us-0.15.zip > assets/vosk-model-small-en-us-0.15.zip

# Setup library for BNF

# Download library from https://github.com/sylvainhalle/Bullwinkle/releases/tag/v1.4.6
curl https://github.com/sylvainhalle/Bullwinkle/releases/download/v1.4.6/bullwinkle.jar > bullwinkle.jar

# Setup maven repository 
mvn install:install-file -DgroupId=com.bullwinkle -DartifactId=bullwinkle -Dversion=1.4.6 -Dpackaging=jar -Dfile=$(pwd)/bullwinkle.jar

# Install packages

mvn install

# Start backend

./mvnw spring-boot:run
```

3. Start frontend
```
cd frontend/
npm i
npm start
```
